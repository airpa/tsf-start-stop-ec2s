import boto3


def main(event, context):
    '''
    main takes query parameter input from an API Gateway event,
    and starts or stops an EC2 with region and instanceId and action query 
    parameters.
    '''
    region = event["queryStringParameters"]["region"]
    instanceId = [event["queryStringParameters"]["instanceId"]]
    action = event["queryStringParameters"]["action"]
    ec2 = boto3.client('ec2', region_name=region)
    
    if action == 'start':
        ec2.start_instances(InstanceIds=instanceId)
        return {
            'statusCode': 200,
            'body': 'EC2 ' + instanceId[0] + ' in region ' + region + ' is starting.'
    }
    elif action == 'stop':
        ec2.stop_instances(InstanceIds=instanceId)
        return {
            'statusCode': 200,
            'body': 'EC2 ' + instanceId[0] + ' in region ' + region + ' is stopping.'
    }
    else:
        return {
            'statusCode': 200,
            'body': 'Invalid action ' + action + '. Nothing to do.'
    }