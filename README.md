This repo supports the blog post found at https://airpa.io/post/sfw-cicd

The project uses the Serverless Framework to implement a Python AWS Lambda Function that starts or stops EC2s with a REST call to API Gateway. GitLab CI/CD is used to automated integration and deployment pipelines. Please visit the blog post for further details.